package com.cgm.academy.firstProject.car.dao.impl;

import com.cgm.academy.firstProject.car.dao.CarDAO;
import com.cgm.academy.firstProject.car.model.Car;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Slf4j
@Repository
public class CarJDBCDAO implements CarDAO {

    private final static String H2_DB_URL = "jdbc:h2:mem:testdb";
    private final static String H2_DB_USER = "sa";
    private final static String H2_DB_PASS = "";
    private final static String H2_DB_DRIVER = "org.h2.Driver";

    @Override
    public void insert(Car car) {
        // prosty insert...
        String query = "INSERT INTO Car " +
                "VALUES (" + car.getId() + ",'" + car.getRegNo() + "','" + car.getBrand() + "','" + car.getModel() + "');";
        Connection connection = null;
        try {
            // zaladowanie sterownika
            Class.forName(H2_DB_DRIVER).newInstance();
            // ustanowienie polaczenia
            connection = DriverManager.getConnection(H2_DB_URL, H2_DB_USER, H2_DB_PASS);
            // obiekt pozwalający wykonywanie zapytań
            Statement statement = connection.createStatement();
            // wykonanie zapytania zapisanego w query
            statement.executeUpdate(query);
            // zamknięcie połączenia
            statement.close();
            connection.close();
        } catch (InstantiationException| IllegalAccessException e ) {
            // nie udało się załadować sterownika - wyjątek z metody newInstance
            log.warn(e.getMessage());
        } catch ( ClassNotFoundException e ) {
            // nie udało się załadować sterownika - wyjątek z Class.forName()
            log.warn(e.getMessage());
        } catch ( SQLException e) {
            // problem z wykonaniem operacji lub nawiązaniem połączenia
            log.warn(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    /* nie udało się zamknąć połączenia
                        np. serwer bazy padł podczas,
                        gdy nasza metoda realizowała swoje zadania
                    */
                    log.warn(e.getMessage());
                }
            }
        }
    }

    @Override
    public Car findById(int carId) {
        // prosty select
        String query = "SELECT * FROM Car WHERE Id = ?";
        Connection conn = null;
        try {
            // nawiazanie polaczenia
            conn = DriverManager.getConnection(H2_DB_URL, H2_DB_USER, H2_DB_PASS);
            // obiekt do wykonywania zapytan z parametrami
            PreparedStatement ps = conn.prepareStatement(query);
            // dodanie parametru
            ps.setInt(1, carId);
            Car car = null;
            // rs - obiekt do którego przekierowujemy dane z wykonanego zapytania
            ResultSet rs = ps.executeQuery();
            if (rs.next()) { // jeżeli coś znaleziono, tworzymy obiekt
                car = new Car(
                        rs.getInt("Id"),
                        rs.getString("RegNo"),
                        rs.getString("Brand"),
                        rs.getString("Model")
                );
            }
            // zamykamy polaczenie i zwracamy obiekt
            rs.close();
            ps.close();
            return car;
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            // bez względu na powodzenie lub nie, chcemy zamknąć połączenie
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    log.warn(e.getMessage());
                }
            }
        }
        return null;
    }
}
