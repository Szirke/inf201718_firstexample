package com.cgm.academy.firstProject.car.dao;

import com.cgm.academy.firstProject.car.model.Car;

public interface CarDAO {

    void insert(Car car);

    Car findById(int carId);

}
